//
//  AccountData.swift
//  
//
//  Created by Jaesung on 2019/11/25.
//

import Foundation

public class AccountData {
    public static func signIn(identifier: String?, username: String, useremail: String) -> User {
        let identifier = identifier ?? UUID().uuidString

        save(identifier: identifier, username: username, useremail: useremail)
        autoLoginOption(isOn: true)
        
        return User(identifier: identifier, username: username, useremail: useremail)
    }
    
    public static func signOut() {
        let userDefault = UserDefaults.standard
        userDefault.set(nil, forKey: "PlaceDataKit_account_identifier")
        userDefault.set(nil, forKey: "PlaceDataKit_account_username")
        userDefault.set(nil, forKey: "PlaceDataKit_account_useremail")
        
        autoLoginOption(isOn: false)
    }
    
    private static func save(identifier: String, username: String, useremail: String) {
        let userDefault = UserDefaults.standard
        
        userDefault.set(identifier, forKey: "PlaceDataKit_account_identifier")
        userDefault.set(username, forKey: "PlaceDataKit_account_username")
        userDefault.set(useremail, forKey: "PlaceDataKit_account_useremail")
    }
    
    
    private static func autoLoginOption(isOn: Bool) {
        let userDefault = UserDefaults.standard
        userDefault.set(isOn, forKey: "PlaceDataKit_account_auto_sign_in")
    }
    
    private static func isRequiredAuth() -> Bool {
        let userDefault = UserDefaults.standard
        guard let isAuto = userDefault.value(forKey: "PlaceDataKit_account_auto_sign_in") as? Bool else { return false }
        return isAuto
    }
    
}
