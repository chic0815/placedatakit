//
//  User.swift
//  
//
//  Created by Jaesung on 2019/11/25.
//

import Foundation

public class User {
    let identifier: String
    let username: String
    let useremail: String
    
    public var myPost: [String: Post] = [:]
    public var myPlaces: [String: Place] = [:]
    
    public var isBanned = false
    
    public init(identifier: String, username: String, useremail: String) {
        self.identifier = identifier
        self.username = username
        self.useremail = useremail
    }
    
    public func addMyPlace(_ place: Place) {
        place.followers.append(self.identifier)
        self.myPlaces.updateValue(place, forKey: place.identifier)
    }
    
    public func uploadPost(_ post: Post, to place: Place) {
        place.placePosts.updateValue(post, forKey: post.identifier)
        self.myPost.updateValue(post, forKey: post.identifier)
    }
    
    public func reportPost(_ post: Post) {
        guard post.isReported else { return }
        post.isReported = true
        guard let user = PlaceDataKit.getPoster(identifier: post.userIdentifier) else { return }
        Admin.sendReport(to: user)
    }
}
