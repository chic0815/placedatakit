//
//  DataSender.swift
//  
//
//  Created by 이재성 on 2019/11/25.
//

import Foundation

protocol DataSendable {
    var sender: DataSender { get set }
}

public class DataSender {
    func sendData(payload: [String: Any]) {
        // Save to Firebase
    }
    
    func sendUserData(user: User) {
        var dictionary: [String: User] = [:]
        dictionary.updateValue(user, forKey: user.identifier)
    }
    
    func sendPostData(post: Post) {
        var dictionary: [String: Post] = [:]
        dictionary.updateValue(post, forKey: post.identifier)
    }
    
    func sendPlaceData(place: Place) {
        var dictionary: [String: Place] = [:]
        dictionary.updateValue(place, forKey: place.identifier)
    }
}


// http://myplaces.com/place/post

