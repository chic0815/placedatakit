//
//  Post.swift
//  
//
//  Created by Jaesung on 2019/11/25.
//

import Foundation

public class Post {
    let identifier: String
    let username: String
    let userIdentifier: String  // let email = PlaceDataKit.getPoster(id)?.useremail
    let placeIdentifer: String  // tag
    let imageURL: String
    let date: String
    let likes = 0
    
    // Ban
    public var isReported = false
    
    public init(user: User, place: Place, imageURL: String) {
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        self.identifier = UUID().uuidString
        self.username = user.username
        self.userIdentifier = user.useremail
        self.placeIdentifer = place.identifier
        self.imageURL = imageURL
        self.date = formatter.string(from: currentDate)
    }
}
