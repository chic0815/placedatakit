import Foundation

public class PlaceDataKit {
    public static var current = PlaceDataKit()
    
    public var user: User!
    public var place: [Place]!  // contains post
    
    public static func initiate(with identifier: String) {
        guard let user = fetchUser(identifier: identifier) else { return }
        PlaceDataKit.current.user = user
    }
    
    public static func fetchUser(identifier: String) -> User? {
        guard let username = UserDefaults.standard.value(forKey: "placedatakit_user_username") as? String,
            let useremail = UserDefaults.standard.value(forKey: "placedatakit_user_useremail") as? String else { return nil }
        return User(identifier: identifier, username: username, useremail: useremail)
    }
    
    public static func getPoster(identifier: String) -> User? {
        // Search
        let users: [User] = []
        return users.filter{ $0.identifier == identifier }.first
    }
}
