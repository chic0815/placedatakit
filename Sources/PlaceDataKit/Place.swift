//
//  Place.swift
//  
//
//  Created by Jaesung on 2019/11/25.
//

import MapKit
import AddressBook

public class Place: NSObject {
    // Place Location Info
    let identifier = UUID().uuidString
    public var title: String?
    public var type: String
    public var address: String
    public var coordinate: CLLocationCoordinate2D
    
    // Contact
    public var contact: String?
    
    // Place Post
    public var likes = 0
    public var followers: [String] = []    // User.idenfier
    public var placePosts: [String: Post] = [:]
    
    public init(title: String, type: String, address: String, coordinate: CLLocationCoordinate2D, contact: String?) {
        self.title = title
        self.type = type
        self.address = address
        self.coordinate = coordinate
        self.contact = contact
        
        super.init()
    }
}

extension Place: MKAnnotation {
    
    public var subtitle: String? {
        return address
    }
}
