import XCTest

import PlaceDataKitTests

var tests = [XCTestCaseEntry]()
tests += PlaceDataKitTests.allTests()
XCTMain(tests)
